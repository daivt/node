const pool = require("../config/database");
const bcrypt = require("bcrypt");
const { verify, sign } = require("jsonwebtoken");

module.exports = {
  get: async (id) => {
    const query = {
      text:
        "SELECT account.username, user_info.address, user_info.birthday, user_info.gender,user_info.avatar, user_info.id, user_info.phone " +
        "FROM auth.account inner join auth.user_info ON auth.account.id=auth.user_info.id AND auth.account.id=$1 ",
      values: [id],
    };
    console.log('query',query);
    return await pool.query(query);
  },
  detail: async (params) => {
    const query = {
      text: "SELECT address, birthday, gender, id, phone FROM auth.user_info WHERE id=$1",
      values: [params.id],
    };
    return await pool.query(query);
  },
  update: async (params) => {
    const query = {
      text: "UPDATE auth.user_info SET address=$1, birthday=$2, gender=$3, phone=$4 ,avatar=$5 WHERE id=$6",
      values: [
        params.address,
        params.birthday,
        params.gender,
        params.phone,
        params.avatar,
        params.id,
      ],
    };
    return await pool.query(query);
  },

  updateAvatar: async (req, res) => {
    const params = req.body;
    const data = await verify(req.token, "secretkey");
    const query = {
      text: "UPDATE auth.user_info SET avatar=$1 WHERE id=$2",
      values: [params.avatar, data.id],
    };
    pool.query(query, async (err, result) => {
      if (err) {
        res.json({
          error: err,
        });
      } else {
        res.status(200).json({ message: "ok" });
      }
    });
  },
  store: async (req, res) => {
    const params = req.body;
    const query = {
      text: "INSERT INTO auth.account (username,password) VALUES ($1,$2) RETURNING id;",
      values: [
        params.username,
        bcrypt.hashSync(
          params.password,
          bcrypt.genSaltSync(params.username.length)
        ),
      ],
    };

    pool.query(query, async (err, result) => {
      if (err) {
        res.json({
          error: err,
        });
      } else {
        pool.query(
          {
            text: "INSERT INTO auth.user_info(address, birthday, gender, phone, id) VALUES ($1, $2, $3, $4, $5) RETURNING *;",
            values: [
              params.address,
              params.birthday,
              params.gender,
              params.phone,
              result.rows?.[0]?.id,
            ],
          },
          (e, r) => {
            if (e) {
              res.json({
                error: e,
              });
            } else {
              res.json({
                data: r.rows[0],
              });
            }
          }
        );
      }
    });
  },
  delete: async (params) => {
    const query = {
      text: "DELETE FROM auth.user_info WHERE id=$1",
      values: [params.id],
    };
    return await pool.query(query);
  },
};
