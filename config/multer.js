const moment = require("moment");
const multer = require("multer");

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "public/images");
  },
  filename: function (req, file, cb) {
    cb(null, `${moment().valueOf()}_${file.originalname}`);
  },
});

var upload = multer({ storage: storage });
module.exports = { upload };
