var express = require("express");
const { upload } = require("../config/multer");
var router = express.Router();
const { parse } = require("url");
const axios = require("axios");
const api = require("../config/api");
var http = require("http");
var https = require("https");

/* GET home page. */
router.get("/", function (req, res) {
  res.render("index", { title: "Express" });
});

router.post("/uploadfile", upload.single("file"), (req, res, next) => {
  const file = req.file;
  if (!file) {
    const error = new Error("Please upload a file");
    error.httpStatusCode = 400;
    return next(error);
  }
  res.json({ url: file.filename });
});

router.get("/images/:name", (req) => {
  console.log("req", req.url, parse(req.url, true));
});

router.post("/searchLocation", async (req, res) => {
  const search = req.body.term;
  try {
    https.get(
      `https://maps.googleapis.com/maps/api/place/textsearch/json?key=AIzaSyAUcYqZXv89zmmKsAF1G1-KrqVdecBvC_c&query=${search}`,
      (response) => {
        let data = "";
        response.on("data", (check) => {
          data += check;
        });
        response.on("end", () => {
          res.status(200).json({
            data: JSON.parse(data)
              ?.results?.filter((v) => v.geometry)
              .map((v) => ({
                name: v.name,
                formatted_address: v.formatted_address,
                geometry: v.geometry,
              })),
          });
        });
      }
    );
  } catch (err) {
    console.error("GG", err);
  }
});

module.exports = router;
