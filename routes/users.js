var express = require("express");
const { verify, sign } = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const pool = require("../config/database");
const verifyToken = require("../config/auth");
const { check, validationResult, checkSchema } = require("express-validator");
var router = express.Router();
const userController = require("../controllers/UserController");
const { showMessage } = require("../config/validate");

/* GET users listing. */
router.post("/post", verifyToken, function (req, res, next) {
  verify(req.token, "secretkey", (err, authData) => {
    res.json({ data: "ok", err, authData });
  });
});

router.post(
  "/login",
  [check("password").isLength({ min: 5 }).withMessage("Ít nhất 5 ký t")],
  showMessage,
  async function (req, res, next) {
    const user = req.body;
    const data = await pool.query({
      text: "SELECT * FROM auth.account where account.username=$1",
      values: [user.username],
    });
    if (data.rowCount) {
      const tmp = data.rows?.[0];
      const match = bcrypt.compareSync(user.password, tmp.password);
      if (match) {
        res.json({
          data: tmp,
          token: sign(tmp, "secretkey"),
        });
      } else {
        res.json({
          message: "sai pass",
        });
      }
    } else {
      res.json({
        message: "sai username",
      });
    }
  }
);

router.post(
  "/signup",
  checkSchema({
    password: {
      isLength: { options: { min: 5 }, errorMessage: "Ít nhất 5 ký tự" },
    },
    username: {
      isLength: { options: { min: 5 }, errorMessage: "Ít nhất 5 ký tự" },
      optional: {
        options: { nullable: false },
        errorMessage: "This field is required",
      },
    },
  }),
  showMessage,
  userController.store
);
router.post(
  "/updateAvatar",
  checkSchema({
    avatar: {
      optional: {
        options: { nullable: false },
        errorMessage: "This field is required",
      },
    },
  }),
  showMessage,
  verifyToken,
  userController.updateAvatar
);

router.get("/get/:id", verifyToken, async (req, res) => {
  const data = await userController.get(req.params.id);
  if (data.rowCount) {
    res.status(200).json(data.rows[0]);
  }
  res.status(400).json({ message: "empty" });
});

module.exports = router;
